package echoxray

import (
	"github.com/aws/aws-xray-sdk-go/strategy/sampling"
	"github.com/aws/aws-xray-sdk-go/xray"
)

const samplingRules = `
{
	"version": 1,
	"default": {
	  "fixed_target": 1,
	  "rate": 0.05
	},
	"rules": [
	  {
		"description": "log all post actions",
		"id": "1",
		"service_name": "*",
		"http_method": "POST",
		"url_path": "*",
		"fixed_target": 1,
		"rate": 1
	  },
	  {
		"description": "log all put actions",
		"id": "2",
		"service_name": "*",
		"http_method": "PUT",
		"url_path": "*",
		"fixed_target": 1,
		"rate": 1
	  },
	  {
		"description": "log all delete actions",
		"id": "3",
		"service_name": "*",
		"http_method": "DELETE",
		"url_path": "*",
		"fixed_target": 1,
		"rate": 1
	  },
	  {
		"description": "do not log OPTIONS",
		"id": "4",
		"service_name": "*",
		"http_method": "OPTIONS",
		"url_path": "*",
		"fixed_target": 0,
		"rate": 0
	  },
	  {
		"description": "do not log health",
		"id": "5",
		"service_name": "*",
		"http_method": "GET",
		"url_path": "/health",
		"fixed_target": 0,
		"rate": 0
	  },
	  {
		"description": "do not log smoke",
		"id": "6",
		"service_name": "*",
		"http_method": "GET",
		"url_path": "/smoketest",
		"fixed_target": 0,
		"rate": 0
	  }
	]
  }
`

func Configure(c xray.Config) error {
	if c.SamplingStrategy == nil {
		ss, err := sampling.NewLocalizedStrategyFromJSONBytes([]byte(samplingRules))
		if err != nil {
			return err
		}
		c.SamplingStrategy = ss
	}

	return xray.Configure(c)
}
