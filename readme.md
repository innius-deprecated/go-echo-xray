# go-echo-xray 

[echo](https://echo.labstack.com/) middleware for [aws-xray-sdk-go](https://github.com/aws/aws-xray-sdk-go)

this package provides an HTTP Handler which instruments http requests with xray 

## Quick Start

### Configuration
```
 import (
    "bitbucket.org/to-increase/go-echo-xray"
    "github.com/aws/aws-xray-sdk-go/xray"
 )

 func init() {
    echoxray.Configure(xray.Config{
        DaemonAddr:       "127.0.0.1:2000", // default
        LogLevel:         "info",           // default
        ServiceVersion:   "1.2.3",
    })
 }
```

### HTTP Handler
```

func main() {
  
    var handler = func(c echo.Context) error {
        _, seg := xray.BeginSubsegment(c.Request().Context(), "foo")
        defer seg.Close(nil)

        return echo.NewHTTPError(http.StatusExpectationFailed, "foo")
    }

    func main() {

        e := echo.New()
        e.Use(XRay("myApp"))
        e.GET("/", handler)
        e.Logger.Fatal(e.Start(":1323"))
    }

}

```