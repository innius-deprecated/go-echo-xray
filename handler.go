package echoxray

import (
	"bytes"
	"net"
	"net/http"
	"strconv"
	"strings"

	"github.com/aws/aws-xray-sdk-go/strategy/sampling"

	"bitbucket.org/to-increase/go-feature-flag/features"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"

	"github.com/aws/aws-xray-sdk-go/header"
	"github.com/aws/aws-xray-sdk-go/xray"
	log "github.com/sirupsen/logrus"

	"bitbucket.org/to-increase/go-feature-flag"
)

type HandlerConfig struct {
	// Skipper defines a function to skip middleware.
	Skipper middleware.Skipper

	Name string

	// collection of tags which are added as xray annotations
	Tags map[string]string
}

func skipper(c echo.Context) bool {
	if strings.HasSuffix(c.Request().RequestURI, "/health") {
		return true
	}
	return !featureflags.FeatureEnabled(features.XRAY)
}

var defaultConfig = HandlerConfig{
	Skipper: skipper,
	Tags:    map[string]string{},
}

// Handler returns a middleware which instruments all requests for xray
func Handler(name string) echo.MiddlewareFunc {
	c := defaultConfig
	c.Name = name

	c.Tags["stack"] = featureflags.EnvironmentName()

	return HandlerWithConfig(c)
}

// HandlerWithConfig returns Handler middleware with config.
// See: `Handler()`.
func HandlerWithConfig(config HandlerConfig) echo.MiddlewareFunc {
	// Defaults
	if config.Skipper == nil {
		config.Skipper = defaultConfig.Skipper
	}

	sn := xray.NewFixedSegmentNamer(config.Name)

	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			if config.Skipper(c) {
				return next(c)
			}

			r := c.Request()
			name := sn.Name(r.Host)

			traceHeader := header.FromString(r.Header.Get("x-amzn-trace-id"))

			ctx, seg := xray.NewSegmentFromHeader(r.Context(), name, r, traceHeader)

			r = r.WithContext(ctx)
			for key, value := range config.Tags {
				seg.AddAnnotation(key, value)
			}
			seg.Lock()
			scheme := "https://"
			if r.TLS == nil {
				scheme = "http://"
			}
			seg.GetHTTP().GetRequest().Method = r.Method
			seg.GetHTTP().GetRequest().URL = scheme + r.Host + r.URL.Path
			seg.GetHTTP().GetRequest().ClientIP, seg.GetHTTP().GetRequest().XForwardedFor = clientIP(r)
			seg.GetHTTP().GetRequest().UserAgent = r.UserAgent()

			// Don't use the segment's header here as we only want to
			// send back the root and possibly sampled values.
			var respHeader bytes.Buffer
			respHeader.WriteString("Root=")
			respHeader.WriteString(seg.TraceID)

			if traceHeader.SamplingDecision != header.Sampled && traceHeader.SamplingDecision != header.NotSampled {
				req := &sampling.Request{Host: r.Host, Method: r.Method, URL: r.URL.Path}
				seg.Sampled = seg.ParentSegment.GetConfiguration().SamplingStrategy.ShouldTrace(req).Sample
				log.Debugf("SamplingStrategy decided: %t", seg.Sampled)
			}
			if traceHeader.SamplingDecision == header.Requested {
				respHeader.WriteString(";Sampled=")
				respHeader.WriteString(strconv.Itoa(btoi(seg.Sampled)))
			}

			c.Response().Header().Set("x-amzn-trace-id", respHeader.String())
			seg.Unlock()

			c.SetRequest(r)
			err := next(c)
			resp := c.Response()

			status := resp.Status
			if httpError, ok := err.(*echo.HTTPError); ok {
				status = httpError.Code
			}

			seg.Lock()
			seg.GetHTTP().GetResponse().Status = status
			seg.GetHTTP().GetResponse().ContentLength, _ = strconv.Atoi(resp.Header().Get("Content-Length"))

			if status >= 400 && status < 500 {
				seg.Error = true
			}
			if status == 429 {
				seg.Throttle = true
			}
			if status >= 500 && status < 600 {
				seg.Fault = true
			}

			seg.Unlock()
			seg.Close(nil)
			return err
		}
	}
}

func clientIP(r *http.Request) (string, bool) {
	forwardedFor := r.Header.Get("X-Forwarded-For")
	if forwardedFor != "" {
		return strings.TrimSpace(strings.Split(forwardedFor, ",")[0]), true
	}
	ip, _, err := net.SplitHostPort(r.RemoteAddr)
	if err != nil {
		return r.RemoteAddr, false
	}
	return ip, false
}

func btoi(b bool) int {
	if b {
		return 1
	}
	return 0
}

func parseHeaders(h http.Header) map[string]string {
	m := map[string]string{}
	s := h.Get("x-amzn-trace-id")
	for _, c := range strings.Split(s, ";") {
		p := strings.SplitN(c, "=", 2)
		k := strings.TrimSpace(p[0])
		v := ""
		if len(p) > 1 {
			v = strings.TrimSpace(p[1])
		}
		m[k] = v
	}
	return m
}
